const jwt = require("jsonwebtoken");
const Router = require("express-promise-router");
const router = new Router();
const routes = require("./routes.js");

// Public routes
router.post("/auth", routes.auth);

// Token decoding
router.use(decodeToken);

// Routes
router.post("/", routes.create);

function decodeToken(req, res, next){
	try{
		const token = req.headers["authorization"].split(" ")[1];
		const decoded = jwt.verify(token, process.env.JWT_PUBLIC_KEY, {
			algorithm: ["RS256"]
		});
		req.user = {
			_id: decoded._id
		};
		next();
	}catch(error){
		res.status(401).send({ message: "INVALID_TOKEN" });
	}
}

module.exports = router;
