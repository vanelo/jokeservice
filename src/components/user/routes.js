const models = require("../models.js");
const bcrypt = require("bcrypt");
const randToken = require("rand-token");
const jwt = require("jsonwebtoken");

module.exports = {
	create,
	auth
};

async function create(req, res){
	// Validate
	req.checkBody("name").isLength({min: 2, max:200});
	req.checkBody("email").isEmail();
	req.checkBody("password").isLength({min: 2, max:200});
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){
		res.status(400).send({message: "INVALID_"+ validationResult.array()[0].param.toUpperCase()});
		return;
	}

	// Generate password hash
	const passwordHash = await bcrypt.hash(req.body.password, 10);

	// Create refresh token
	const refreshToken = randToken.uid(256);

	// Create user
	let user;
	try{
		user = await models.User.create({
			name: req.body.name,
			email: req.body.email,
			passwordHash: passwordHash,
			creationDate: Date.now(),
			createdBy: {
				_id: req.user._id
			},
			refreshToken: refreshToken
		});
	}catch(error){
		if(error.code === 11000){
			res.sendStatus(409);
			return;
		}
	}
	// Send response
	res.status(201).send({_id: user._id.toString()});
}

async function auth(req, res){
	// Validate
	if(req.body.refreshToken){
		req.checkBody("refreshToken").isLength({min:3, max: 300});
	}else{
		req.checkBody("email").isEmail();
		req.checkBody("password").isLength({min:3, max: 200});
	}
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){
		res.status(400).send({message: "INVALID_"+ validationResult.array()[0].param.toUpperCase()});
		return;
	}

	// Create new refresh token
	const refreshToken = randToken.uid(256);

	// Update user
	var user;
	if(req.body.refreshToken){
		user = await models.User.findOne({refreshToken: req.body.refreshToken}).exec();
		if(!user){
			res.status(400).send({ message: "INVALID_REFRESH_TOKEN" });
			return;
		}
	}else{
		user = await models.User.findOne({email: req.body.email.toLowerCase()}).exec();
		if(!user){
			res.status(400).send({ message: "EMAIL_NOT_FOUND" });
			return;
		}
		const validPassword = await bcrypt.compare(req.body.password, user.passwordHash);
		if(!validPassword){
			res.status(400).send({ message: "INVALID_PASSWORD" });
			return;
		}
	}

	// Create access token
	const accessToken = jwt.sign({
		aud: "AUTH",
		_id: user._id,
	}, process.env.JWT_PRIVATE_KEY, {
		expiresIn: "1h",
		algorithm: "RS256"
	});

	// Save new refresh token
	user.refreshToken = refreshToken;
	await user.save();

	// Send response
	res.status(200).send({
		accessToken: accessToken,
		refreshToken: refreshToken,
		name: user.name
	});
}