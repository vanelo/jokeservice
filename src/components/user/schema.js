const mongoose = require("mongoose");

const lessonSchema = new mongoose.Schema({
	name: {type: String, required: true},
	email: {type: String, required: true, unique: true},
	passwordHash: {type: String, required: true},
	creationDate: {type: Date, required: true},
	createdBy: {
		_id: {type: mongoose.Schema.Types.ObjectId}
	},
	refreshToken: {type: String}
});

module.exports = lessonSchema;