const mongoose = require("mongoose");
const lessonLearnedSchema = require("./lessonLearned/schema.js");
const userSchema = require("./user/schema.js");
var mongooseConnection;
var output = {
	setup,
	disconnect	
};
module.exports = output;

async function setup(config){
	// Setup mongoose
	mongoose.set("useCreateIndex", true);
	mongoose.set("useFindAndModify", false);
	if(mongooseConnection){ mongooseConnection.removeAllListeners(); } //this is to ease testing
	while(!mongooseConnection){
		try{
			mongooseConnection = await mongoose.createConnection(config.dbUrl, {
				user: config.user,
				pass: config.password,
				useNewUrlParser: true,
				useUnifiedTopology: true,
				reconnectTries: Number.MAX_VALUE, 	// Never stop trying to reconnect (after initial successful connection)
				reconnectInterval: 500 			// Attempt to reconnect every 500 ms
			});
		}catch(error){
			await new Promise(r => setTimeout(r, 1000));
			continue;
		}
	}
	mongooseConnection.on("error", (error)=> {
		console.error("Error in MongoDB connection: " + error);
		throw error;
	});
	mongooseConnection.on("disconnected", () => console.warn("Disconnected from MongoDB"));

	// Setup models
	output.LessonLearned = mongooseConnection.model("LessonLearned", lessonLearnedSchema);
	output.User = mongooseConnection.model("User", userSchema);

	// Wait for indexes to be ready
	await Promise.all(
		Object
			.keys(output)
			.filter(k => !["setup", "disconnect"].includes(k))
			.map(k => output[k].init())
	);
}

async function disconnect(){
	await mongooseConnection.close();
	mongooseConnection =null;
}