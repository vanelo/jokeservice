const jwt = require("jsonwebtoken");
const Router = require("express-promise-router");
const routes = require("./routes.js");
const router = new Router();

// Token decoding
router.use(decodeToken);

// Routes
router.post("/", routes.create);
router.patch("/:id", routes.edit);
router.get("/", routes.getLessonLearned);
router.delete("/:id", routes.deleteLessonLearned);

function decodeToken(req, res, next)
{
	try{
		const token = req.headers["authorization"].split(" ")[1];
		const decoded = jwt.verify(token, process.env.JWT_PUBLIC_KEY, {
			algorithm: ["RS256"]
		});
		req.user = {
			_id: decoded._id
		};
		next();
	}catch(error){
		res.status(401).send({ message: "INVALID_TOKEN" });
	}
}

module.exports = router;
