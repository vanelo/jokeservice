const validator = require("validator");
const models = require("../models.js");

module.exports = {
	create,
	edit,
	getLessonLearned,
	deleteLessonLearned
};

async function create(req, res){
	// Validate
	req.checkBody("keywords").isLength({min: 3, max: 400});
	req.checkBody("title").isLength({min: 3, max: 300});
	req.checkBody("problem").isLength({min: 3, max: 500});
	req.checkBody("solution").isLength({min: 3, max: 500});
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){
		res.status(400).send({message: "INVALID_"+ validationResult.array()[0].param.toUpperCase()});
		return;
	}

	// Prepare the keywords
	const keywords = req.body.keywords.split(",").map(k=> k.trim().toLowerCase());

	// Create a lessonLearned
	const lessonLearned = await models.LessonLearned.create({
		keywords,
		title: req.body.title,
		problem: req.body.problem,
		solution: req.body.solution,
		creationDate: Date.now(),
		createdBy: {
			_id: req.user._id
		}
	});

	// Send Response
	res.status(201).send({
		_id: lessonLearned._id.toString()
	});
}

async function edit(req, res){
	// Validate
	req.checkParams("id").isMongoId();
	req.checkBody("keywords").isLength({min: 2, max: 200}).optional();
	req.checkBody("title").isLength({min: 3, max: 300}).optional();
	req.checkBody("problem").isLength({min: 3, max: 500}).optional();
	req.checkBody("solution").isLength({min: 3, max: 500}).optional();
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){
		res.status(400).send({message: "INVALID_"+ validationResult.array()[0].param.toUpperCase()});
		return;
	}
	if(!(Object.keys(req.body).length > 0)){
		res.status(400).send({message: "INVALID_BODY"});
		return;	
	}

	// Prepare object
	const obj = {};
	if(req.body.keywords){
		obj.keywords = req.body.keywords.split(",").map(k=> k.trim().toLowerCase());
	}
	if(req.body.title){
		obj.title = req.body.title;
	}
	if(req.body.problem){
		obj.problem = req.body.problem;
	}
	if(req.body.solution){
		obj.solution = req.body.solution;
	}

	// Update lesson learned
	const lessonLearned = await models.LessonLearned.findOneAndUpdate({_id: req.params.id}, {
		$set: obj
	}, {new: true});

	// Send response
	if(lessonLearned){
		res.sendStatus(200);
	}else{
		res.sendStatus(404);
	}
}

async function getLessonLearned(req, res){
	// Validate
	req.checkQuery("keywords").isLength({min: 2}).optional(); //example: keywords=mongo,code11000,nodejs
	req.checkQuery("q").isLength({min: 3}).optional(); //example: q=Error code 11000 (searching in the title and problem)
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){
		res.status(400).send({message: "INVALID_"+ validationResult.array()[0].param.toUpperCase()});
		return;
	}

	// Prepare query
	const query = {};
	if(req.query.keywords){
		query["keywords"] = {$in: req.query.keywords.split(",").map(t=> t.trim().toLowerCase())};
	}
	if(req.query.q){
		query["$or"] = [
			{title: new RegExp(req.query.q.trim(), "i")},
			{problem: new RegExp(req.query.q.trim(), "i")}
		];
	}

	const lessons = await models.LessonLearned
		.find(query)
		.select(["keywords", "title", "problem", "solution", "creationDate"])
		.sort({creationDate: -1})
		.limit(20)
		.lean();

	// Respond with the list of matched lessons
	res.status(200).send(lessons);
}

async function deleteLessonLearned(req, res){
	// Validate
	if(!validator.isMongoId(req.params.id)){
		res.status(400).send({message: "INVALID_ID"});
		return;
	}

	// Remove the lessonLearned selected
	const removedLessonLearned = await models.LessonLearned.findOneAndRemove({_id: req.params.id}, {new: true});

	// Send response
	if(removedLessonLearned){
		res.sendStatus(200);
	}else{
		res.sendStatus(404);
	}
}