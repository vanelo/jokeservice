const mongoose = require("mongoose");

const lessonLearnedSchema = new mongoose.Schema({
	keywords: [{type: String}],
	title: {type: String},
	problem: {type: String},
	solution: {type: String},
	creationDate: {type: Date, required: true},
	createdBy: {
		_id: {type: mongoose.Schema.Types.ObjectId}
	}
});

module.exports = lessonLearnedSchema;