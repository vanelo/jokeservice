const express = require("express");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const http = require("http");
const lessonLearnedRouter = require("./components/lessonLearned/router.js");
const userRouter = require("./components/user/router.js");

module.exports = function(config){
	// Setup express
	const app = express();
	app.use(bodyParser.json());
	app.use(expressValidator());
	app.set("DOMAIN", config.domain);

	// Setup server
	const server = http.Server(app);

	// Setup routers
	app.use("/lessonLearned", lessonLearnedRouter);
	app.use("/user", userRouter);

	// Handle errors
	app.use(async(error, req, res)=> {
		// Dont log client syntax errors
		if(error instanceof SyntaxError){ res.sendStatus(400); return;}

		// Send response
		res.sendStatus(500);
		console.error(error);
	});

	return {
		server: server,
		expressApp: app
	};
};