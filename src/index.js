const App = require("./app.js");
const models = require("./components/models.js");

(async function(){
	// Setup models
	await models.setup({
		dbUrl: process.env.MONGO_URL,
		user: process.env.MONGO_USER,
		password: process.env.MONGO_PASSWORD
	});

	// Setup webServer
	const app = new App({
		domain: process.env.DOMAIN_NAME
	});

	// Start listening for connections
	app.server.listen(3000);
})().catch(error => {
	// Log error
	throw error;
});