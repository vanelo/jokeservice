const Docker = require("dockerode");
const MongoClient = require("mongodb").MongoClient;
const models = require("../src/components/models.js");
const App = require("../src/app.js");
const docker = new Docker();
var app;
var services = {
	mongoStore: {
		url: "mongodb://127.0.0.1:27017/lessonLearned"
	}
};

module.exports = {
	setupServices,
	resetServices,
	stopServices,
	setupApp,
	stopApp
};

async function setupServices(){
	services.mongoStore.container = await setupMongoStore();

	// Connect client
	while(!services.mongoStore.client){
		try{
			services.mongoStore.client = await MongoClient.connect(services.mongoStore.url, {useNewUrlParser: true, useUnifiedTopology: true});
		}catch(error){
			await new Promise(r=>setTimeout(r, 500));
			continue;
		}
	}
}

async function setupMongoStore(){
	// Download image if not availble
	const imageName = "mongo:4.0.5";
	const haveImage = (await docker.listImages()).some(image => (image.RepoTags && image.RepoTags.includes(imageName)));
	if(!haveImage){
		console.info("Downloading " + imageName);
		const stream = await docker.pull(imageName);
		await new Promise(async(r) => docker.modem.followProgress(stream, r));
	}

	// Re-use container if already up
	const containerName = "lessonLearned-store-mongo";
	const containers = await docker.listContainers({all: true});
	const containerInfo = await containers.find(container => container.Names.includes("/"+containerName));
	var container;
	if(containerInfo){
		container = docker.getContainer(containerInfo.Id);
		if(containerInfo.State == "exited"){await container.start();}
	}

	// Create and start container if one doesnt already exist
	if(!container){
		container = await docker.createContainer({
			Image: imageName,
			ExposedPorts: {"27017/tcp": {}},
			HostConfig: {
				PortBindings: {"27017/tcp": [{ HostPort: "27017"}]},
				Mounts: [
					{Target: "/data/db", Type: "tmpfs"},
					{Target: "/data/configdb", Type: "tmpfs"}
				]
			},
			name: containerName
		});
		await container.start();
	}

	// Return container
	return container;
}

async function resetServices(){
	// Reset mongo service
	const collections = await services.mongoStore.client.db("lessonLearned").collections();
	await Promise.all(collections.map(c => c.deleteMany()));
}

async function stopServices(){
	// Disconnect clients
	await services.mongoStore.client.close();

	// Stop services
	await services.mongoStore.container.kill();
	await services.mongoStore.container.remove();
}

async function setupApp(){
	// Setup models
	await models.setup({dbUrl: services.mongoStore.url});

	// Setup web server
	app = new App({
		jwtKeys: {
			private: process.env.JWT_PRIVATE_KEY,
			public: process.env.JWT_PUBLIC_KEY
		},
		domain: "example.com"
	});

	app.server.listen(3000);
}

async function stopApp(){
	// Disconnect models
	await models.disconnect();

	// Stop app
	await new Promise (r => app.server.close(r));
}