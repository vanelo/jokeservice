const request = require("supertest");
const chai = require("chai");
const expect = chai.expect;
const models = require("../../../src/components/models.js");
const helper = require("../../helper.js");
const bcrypt = require("bcrypt");

describe("/user/auth POST", async function(){
	var agent;
	var accessToken;
	var user;
	const obj = {
		email: "vanessa@lopez.ro",
		password: "123456"
	};

	before(async function(){
		// Create agent
		agent = request.agent("http://127.0.0.1:3000");
	});

	beforeEach(async function(){
		// Reset services
		await helper.resetServices();

		// Generate the password hash
		const passwordHash = await bcrypt.hash(obj.password, 10);

		// Create a user
		user = await models.User.create({
			name: "Amelia",
			email: obj.email,
			passwordHash: passwordHash,
			creationDate: Date.now(),
			createdBy: {
				_id: "507f191e810c19729de860ee"
			},
			refreshToken: "asdkfhalsjdhfkajsdhljaslfkdgaseryi"
		});
	});

	it("Should respond with 200 and create a user", async function(){
		await agent
			.post("/user/auth")
			.send({refreshToken: user.refreshToken})
			.expect(200)
			.then(async res => {
				// Verify the response
				expect(res.body.refreshToken).to.exist;
				expect(res.body.accessToken).to.exist;
				expect(res.body.name).to.equal(user.name);

				// Verify the user collection
				const thisUser = await models.User.findOne({refreshToken: res.body.refreshToken}).exec();
				expect(thisUser.refreshToken).to.not.equal(user.refreshToken);
			});

		await agent
			.post("/user/auth")
			.send(obj)
			.expect(200)
			.then(async res => {
				// Verify the response
				expect(res.body.refreshToken).to.exist;
				expect(res.body.accessToken).to.exist;
				expect(res.body.name).to.equal(user.name);

				// Verify the user collection
				const thisUser = await models.User.findOne({_id: user._id}).exec();
				expect(thisUser.refreshToken).to.not.equal(user.refreshToken);
			});
	});

	it("Should respond with 400 when provided with invalid attributes", async function(){
		await agent
			.post("/user/auth")
			.set({"Authorization": "Bearer " + accessToken})
			.send({password: obj.password})
			.expect(400);

		await agent
			.post("/user/auth")
			.set({"Authorization": "Bearer " + accessToken})
			.send({email: obj.email})
			.expect(400);

		await agent
			.post("/user/auth")
			.set({"Authorization": "Bearer " + accessToken})
			.send({email: obj.email, password: "124578"})
			.expect(400);

		// Verify the user collection
		const thisUser = await models.User.findOne({}).exec();
		expect(thisUser.refreshToken).to.equal(user.refreshToken);
	});
});
