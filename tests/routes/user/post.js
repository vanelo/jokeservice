const request = require("supertest");
const chai = require("chai");
const expect = chai.expect;
const models = require("../../../src/components/models.js");
const helper = require("../../helper.js");
const jwt = require("jsonwebtoken");

describe("/user POST", async function(){
	var agent;
	var accessToken;
	const obj = {
		name: "Vanessa",
		email: "vanessa@lopez.ro",
		password: "123456"
	};

	before(async function(){
		// Create agent
		agent = request.agent("http://127.0.0.1:3000");

		// Create accessToken
		accessToken = jwt.sign({
			aud: "AUTH",
			_id: "507f191e810c19729de860ea"
		}, process.env.JWT_PRIVATE_KEY,{
			expiresIn: "1h",
			algorithm: "RS256"
		});
	});

	beforeEach(async function(){
		// Reset services
		await helper.resetServices();
	});

	it("Should respond with 201 and create a user", async function(){
		await agent
			.post("/user")
			.set({"Authorization": "Bearer " + accessToken})
			.send(obj)
			.expect(201)
			.then(async res => {
				// Verify the response
				expect(res.body._id).to.exist;

				// Verify the user collection
				const user = await models.User.findOne({_id: res.body._id}).exec();
				expect(user._id).to.exist;
				expect(user.name).to.equal(obj.name);
				expect(user.email).to.equal(obj.email);
				expect(user.passwordHash).to.exist;
				expect(user.creationDate).to.be.a("date");
				expect(user.createdBy._id.toString()).to.equal("507f191e810c19729de860ea");
			});
	});

	it("Should respond with 400 when provided with invalid attributes", async function(){
		await agent
			.post("/user")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {name: null}))
			.expect(400);

		await agent
			.post("/user")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {email: null}))
			.expect(400);

		await agent
			.post("/user")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {password: null}))
			.expect(400);

		// Verify the user collection
		const users = await models.User.find({}).exec();
		expect(users.length).to.equal(0);
	});

	it("Should respond 401 when the accessToken expired", async function(){
		const expiredToken = jwt.sign({
			aud: "AUTH", 
			_id: "507f191e810c19729de860ea",
			exp: 1000
		}, process.env.JWT_PRIVATE_KEY, {
			algorithm: "RS256"
		});

		await agent
			.post("/user")
			.set({"Authorization": "Bearer " + expiredToken})
			.send(obj)
			.expect(401);
	});

	it("Should respond with 409 when email already exists", async function(){
		// Create a user
		await models.User.create({
			name: "Amelia",
			email: "amelia@asd.com",
			passwordHash: "sadfasd12f35sdf",
			creationDate: Date.now(),
			createdBy: {
				_id: "507f191e810c19729de860ee"
			}
		});
		await agent
			.post("/user")
			.set({"Authorization": "Bearer " + accessToken})
			.send({
				name: "Amelia",
				email: "amelia@asd.com",
				password: "123456"
			})
			.expect(409)
			.then(async () => {
				// Verify the user collection
				const users = await models.User.find({}).exec();
				expect(users.length).to.equal(1);
			});
	});
});
