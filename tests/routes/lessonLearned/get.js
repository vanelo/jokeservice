const request = require("supertest");
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const expect = chai.expect;
const jwt = require("jsonwebtoken");
const models = require("../../../src/components/models.js");
const helper = require("../../helper.js");

// Setup chai
chai.use(sinonChai);

describe("/lessonLearned?keywords={category}&q={q} GET", async function(){
	var agent;
	var accessToken;

	before(async function(){
		// Create agent
		agent = request.agent("http://127.0.0.1:3000");

		// Create accessToken
		accessToken = jwt.sign({
			aud: "AUTH",
			_id: "507f191e810c19729de860ea"
		}, process.env.JWT_PRIVATE_KEY,{
			expiresIn: "1h",
			algorithm: "RS256"
		});
	});

	beforeEach(async function(){
		// Reset services
		await helper.resetServices();

		// Create lessons learned
		await models.LessonLearned.create({
			keywords: ["it", "iot"],
			title: "Automate process",
			problem: "Consumption reduction",
			solution: "Server and nodes working",
			creationDate: Date.now(),
			createdBy: {
				_id: "507f191e810c19729de860ea"
			}
		});
		await models.LessonLearned.create({
			keywords: ["education", "iot"],
			title: "Automate process in education",
			problem: "Consumption reduction",
			solution: "Server and nodes working",
			creationDate: Date.now(),
			createdBy: {
				_id: "507f191e810c19729de860ea"
			}
		});
	});

	after(function(){
		// Restore functions
		sinon.restore();
	});

	it("Should respond with 200 and the list of matched lessons Learned", async function(){
		await agent
			.get("/lessonLearned")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200)
			.then(res => {
				expect(res.body.length).to.equal(2);
			});

		await agent
			.get("/lessonLearned?keywords=IOT,IT&q=automate")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200)
			.then(res => {
				expect(res.body.length).to.equal(2);
			});

		await agent
			.get("/lessonLearned?q=Consumption")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200)
			.then(res => {
				expect(res.body.length).to.equal(2);
			});

		await agent
			.get("/lessonLearned?keywords=IT")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200)
			.then(res => {
				expect(res.body.length).to.equal(1);
			});

		await agent
			.get("/lessonLearned?keywords=iot")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200)
			.then(res => {
				expect(res.body.length).to.equal(2);
			});

		await agent
			.get("/lessonLearned?keywords=IOT,IT&q=education")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200)
			.then(res => {
				expect(res.body.length).to.equal(1);
			});
	});

	it("Should respond 401 when the accessToken expired", async function(){
		const expiredToken = jwt.sign({
			aud: "AUTH", 
			_id: "507f191e810c19729de860ea",
			exp: 1000
		}, process.env.JWT_PRIVATE_KEY, {
			algorithm: "RS256"
		});

		await agent
			.get("/lessonLearned")
			.set({"Authorization": "Bearer " + expiredToken})
			.expect(401);
	});
});
