const request = require("supertest");
const chai = require("chai");
const sinonChai = require("sinon-chai");
const expect = chai.expect;
const models = require("../../../src/components/models.js");
const helper = require("../../helper.js");
const jwt = require("jsonwebtoken");

// Setup chai
chai.use(sinonChai);

describe("/lessonLearned/{id} PATCH", async function(){
	var agent;
	var accessToken;
	var lessonLearned;
	const obj = {
		keywords: "it,iot",
		title: "Automate process",
		problem: "Consumption reduction",
		solution: "Server and nodes working"
	};

	before(async function(){
		// Create agent
		agent = request.agent("http://127.0.0.1:3000");

		// Create accessToken
		accessToken = jwt.sign({
			aud: "AUTH",
			_id: "507f191e810c19729de860ea"
		}, process.env.JWT_PRIVATE_KEY,{
			expiresIn: "1h",
			algorithm: "RS256"
		});
	});

	beforeEach(async function(){
		// Reset services
		await helper.resetServices();

		// Create a lessonLearned
		lessonLearned = await models.LessonLearned.create({
			keywords: ["IT", "IOT", "Robots"],
			title: "Automate",
			problem: "Consumption",
			solution: "Server",
			creationDate: Date.now(),
			createdBy: {
				_id: "507f191e810c19729de860ea"
			}
		});
	});

	it("Should respond with 200 and update the lessonLearned when provided with valid params", async function(){
		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.send(obj)
			.expect(200);

		// Verify the lessonLearned collection
		lessonLearned = await models.LessonLearned.findOne({}).exec();
		expect(lessonLearned._id).to.exist;
		expect(lessonLearned.keywords.length).to.equal(2);
		expect(lessonLearned.keywords[0]).to.equal(obj.keywords.split(",")[0]);
		expect(lessonLearned.keywords[1]).to.equal(obj.keywords.split(",")[1]);
		expect(lessonLearned.title).to.equal(obj.title);
		expect(lessonLearned.problem).to.equal(obj.problem);
		expect(lessonLearned.solution).to.equal(obj.solution);
	});

	it("Should respond with 400 when provided with invalid attributes", async function(){
		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {keywords: ""}))
			.expect(400);

		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {title: ""}))
			.expect(400);

		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {problem: ""}))
			.expect(400);

		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {solution: ""}))
			.expect(400);

		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.expect(400);
	});

	it("Should respond 401 when the accessToken expired", async function(){
		const expiredToken = jwt.sign({
			aud: "AUTH", 
			_id: "507f191e810c19729de860ea",
			exp: 1000
		}, process.env.JWT_PRIVATE_KEY, {
			algorithm: "RS256"
		});

		await agent
			.patch("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + expiredToken})
			.send(obj)
			.expect(401);
	});

	it("Should respond with 404 when the lessonLearned was not found", async function(){
		await agent
			.patch("/lessonLearned/507f1f77bcf86cd799439011")
			.set({"Authorization": "Bearer " + accessToken})
			.send({keywords: "IT"})
			.expect(404);
	});
});
