const request = require("supertest");
const chai = require("chai");
const expect = chai.expect;
const models = require("../../../src/components/models.js");
const helper = require("../../helper.js");
const jwt = require("jsonwebtoken");

describe("/lessonLearned POST", async function(){
	var agent;
	var accessToken;
	const obj = {
		keywords: "ti, iot",
		title: "Automate process",
		problem: "Consumption reduction",
		solution: "Server and nodes working"
	};

	before(async function(){
		// Create agent
		agent = request.agent("http://127.0.0.1:3000");

		// Create accessToken
		accessToken = jwt.sign({
			aud: "AUTH",
			_id: "507f191e810c19729de860ea"
		}, process.env.JWT_PRIVATE_KEY,{
			expiresIn: "1h",
			algorithm: "RS256"
		});
	});

	beforeEach(async function(){
		// Reset services
		await helper.resetServices();
	});

	it("Should respond with 201 and create a lessonLearned", async function(){
		await agent
			.post("/lessonLearned")
			.set({"Authorization": "Bearer " + accessToken})
			.send(obj)
			.expect(201)
			.then(async res => {
				// Verify the response
				expect(res.body._id).to.exist;

				// Verify the lessonLearned collection
				const lessonLearned = await models.LessonLearned.findOne({}).exec();
				expect(lessonLearned._id).to.exist;
				expect(lessonLearned.keywords.length).to.equal(2);
				expect(lessonLearned.title).to.equal(obj.title);
				expect(lessonLearned.problem).to.equal(obj.problem);
				expect(lessonLearned.solution).to.equal(obj.solution);
				expect(lessonLearned.creationDate).to.be.a("date");
				expect(lessonLearned.createdBy._id.toString()).to.equal("507f191e810c19729de860ea");
			});
	});

	it("Should respond with 400 when provided with invalid attributes", async function(){
		await agent
			.post("/lessonLearned")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {keywords: null}))
			.expect(400);

		await agent
			.post("/lessonLearned")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {title: null}))
			.expect(400);

		await agent
			.post("/lessonLearned")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {problem: null}))
			.expect(400);

		await agent
			.post("/lessonLearned")
			.set({"Authorization": "Bearer " + accessToken})
			.send(Object.assign(obj, {solution: null}))
			.expect(400);

		// Verify the lessonLearned collection
		const lessonLearneds = await models.LessonLearned.find({}).exec();
		expect(lessonLearneds.length).to.equal(0);
	});

	it("Should respond 401 when the accessToken expired", async function(){
		const expiredToken = jwt.sign({
			aud: "AUTH", 
			_id: "507f191e810c19729de860ea",
			exp: 1000
		}, process.env.JWT_PRIVATE_KEY, {
			algorithm: "RS256"
		});

		await agent
			.post("/lessonLearned")
			.set({"Authorization": "Bearer " + expiredToken})
			.send({
				keywords: "TI, IOT",
				title: "Automate process",
				problem: "Consumption reduction",
				solution: "Server and nodes working"
			})
			.expect(401);
	});
});
