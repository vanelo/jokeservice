const request = require("supertest");
const chai = require("chai");
const sinonChai = require("sinon-chai");
const expect = chai.expect;
const models = require("../../../src/components/models.js");
const helper = require("../../helper.js");
const jwt = require("jsonwebtoken");

// Setup chai
chai.use(sinonChai);

describe("/lessonLearned/{id} DELETE", async function(){
	var agent;
	var accessToken;
	var lessonLearned;

	before(async function(){
		// Create agent
		agent = request.agent("http://127.0.0.1:3000");

		// Create accessToken
		accessToken = jwt.sign({
			aud: "AUTH",
			_id: "507f191e810c19729de860ea"
		}, process.env.JWT_PRIVATE_KEY, {
			expiresIn: "1h",
			algorithm: "RS256"
		});
	});

	beforeEach(async function(){
		// Reset services
		await helper.resetServices();

		// Create a lessonLearned
		lessonLearned = await models.LessonLearned.create({
			keywords: ["IT", "IOT"],
			title: "Automate process",
			problem: "Consumption reduction",
			solution: "Server and nodes working",
			creationDate: Date.now(),
			createdBy: {
				_id: "507f191e810c19729de860ea"
			}
		});
	});

	it("Should respond with 200 and delete the lessonLearned", async function(){
		await agent
			.delete("/lessonLearned/"+ lessonLearned._id.toString())
			.set({"Authorization": "Bearer " + accessToken})
			.expect(200);

		// Verify the lessonLearned collection
		lessonLearned = await models.LessonLearned.findOne({_id: lessonLearned._id}).exec();
		expect(lessonLearned).to.be.null;
	});

	it("Should respond with 400 when provided with invalid id", async function(){
		await agent
			.delete("/lessonLearned/123")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(400);
	});

	it("Should respond 401 when the accessToken expired", async function(){
		const expiredToken = jwt.sign({
			aud: "AUTH", 
			_id: "507f191e810c19729de860ea",
			exp: 1000
		}, process.env.JWT_PRIVATE_KEY, {
			algorithm: "RS256"
		});

		await agent
			.get("/lessonLearned")
			.set({"Authorization": "Bearer " + expiredToken})
			.expect(401);
	});

	it("Should respond with 404 when the lessonLearned doesn't exist", async function(){
		await agent
			.delete("/lessonLearned/507f191e810c19729de860ea")
			.set({"Authorization": "Bearer " + accessToken})
			.expect(404);

		// Verify the lessonLearned collection
		lessonLearned = await models.LessonLearned.findOne({}).exec();
		expect(lessonLearned).to.exist;
	});
});
