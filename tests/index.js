const dotenv = require("dotenv");
const sinon = require("sinon");
const helper = require("./helper.js");

// Read test config from env file
dotenv.config({path: __dirname+"/config.env"});

describe("lessonLearned service", async function(){
	before(async function(){
		// Set timeout
		this.timeout(1000*60*5);

		// Setup external services
		await helper.setupServices();

		// Setup app
		await helper.setupApp();
	});

	beforeEach(function(){
		sinon.restore();
	});

	after(async function(){
		// Set timeout
		this.timeout(1000*30);

		// Stop app
		await helper.stopApp();

		// Stop services
		await helper.stopServices();
	});
	
	describe("Routes", async function(){
		describe("lessonLearned", function(){
			require("./routes/lessonLearned/post.js");
			require("./routes/lessonLearned/patch.{id}.js");
			require("./routes/lessonLearned/delete.{id}.js");
			require("./routes/lessonLearned/get.js");
		});

		describe("User", function(){
			require("./routes/user/post.js");
			require("./routes/user/post-auth.js");
		});
	});
});